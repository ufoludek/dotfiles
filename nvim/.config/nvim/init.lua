-- lazy.nvim plugin manager
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",          -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- lazy keymaps
vim.g.mapleader = " "           -- Make sure to set `mapleader` before lazy so your mappings are correct
vim.g.maplocalleader = "\\"     -- Same for `maplocalleader`

require("lazy").setup({
    {"folke/which-key.nvim"},
    { 
        "folke/neoconf.nvim", 
        cmd = "Neoconf" 
    },

    {"folke/neodev.nvim"},

    -- indent lines
    { 
        "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        config = function()
            require("ibl").setup {
                indent = { char = "┊" }
            }
        end,
    },

    -- auto pair
    {
        "windwp/nvim-autopairs",
        event = "InsertEnter",
        config = true
    },

    -- status line
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        config = function()
            require('lualine').setup {
                options = {
                    section_separators = { left = '', right = ''},
                    component_separators = { left = '', right = '|'}
                }
            }
        end,
    },

    -- colorscheme
    {
        "tomasiser/vim-code-dark",
        lazy = false,
        priority = 1000,
        opts = {},
        config = function()
            vim.opt.termguicolors = true
            vim.cmd([[colorscheme codedark]])
        end,
    },

})

-- cursor
-- vim.o.guicursor = "a:blinkon1"

-- tab settings
vim.opt.tabstop = 4          	-- set the width of a tab to 4 spaces
vim.opt.shiftwidth = 4       	-- set the number of spaces for each indentation level to 4
vim.opt.softtabstop = 4      	-- set the number of spaces for a tab when editing to 4
vim.opt.expandtab = true     	-- use spaces instead of tabs

-- indentation
vim.opt.smartindent = true   	-- automatically adjust the indentation level based on the structure of the previous line

-- general things
vim.opt.number = true			-- show line numbers
vim.opt.relativenumber = true	-- show numbers relative to current line
vim.opt.wrap = true 			-- text wrapping
vim.opt.linebreak = true		-- avoid break in the middle of a word
vim.opt.cursorline = true		-- current line highlight
vim.o.title = true              -- window title

-- syntax
vim.cmd [[syntax enable]]       -- enable syntax highlighting
