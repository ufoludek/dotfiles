# ufoludek's dotfiles

Here's a small collection of most of my dot files I use on most of my devices including desktop and laptop.

Clone repo with all submodules:

```bash
git clone --recurse-submodules --remote-submodules https://gitlab.com/ufoludek/dotfiles.git ~/.dotfiles
```

The file hierarchy is made appropiately for `stow`.

The fastest way to use these dotfiles is `stow DirectoryNameGoesHere`.

For example, if you want to copy the VIM files only:

```bash
cd ~/.dotfiles
stow vim
```

I keep my dotfiles in my home directory in a hidden directory called `.dotfiles`.

Have a nice day.
