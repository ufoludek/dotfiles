"""" vim-plug autoinstall
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"""" vim-plug plugins
call plug#begin('~/.vim/plugged')

" Lightline status line
Plug 'itchyny/lightline.vim'

" Rainbow pairs
Plug 'luochen1990/rainbow'

" Surround
Plug 'tpope/vim-surround'

" Nerd Tree
Plug 'preservim/nerdtree'

" Indentation guidelines
Plug 'yggdroot/indentline'

" Python indentation
"Plug 'Vimjas/vim-python-pep8-indent'
"
"" Markdown
"Plug 'godlygeek/tabular'
"Plug 'preservim/vim-markdown'
"Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

" Code completion, youcompleteme
" Plug 'valloric/youcompleteme'

call plug#end()


"""" Tab Settings
set tabstop=4 " number of visual spaces per TAB
set softtabstop=4 " number of spaces in tab when editing
set expandtab " tabs are spaces
set shiftwidth=4

set autoindent
" set smartindent

"""" Just normal things
set number  " show line numbers
set relativenumber " show numbers relative to current line

set nocompatible 
set wrap " enable text wrapping
set linebreak " avoid break in the middle of a word
set encoding=utf-8 " encoding 
set cursorline  " highlight current line
set wildmenu  " visual autocomplete for command menu
set lazyredraw " redraw only when we need to
set showcmd " show command in bottom bar
set showmatch  " highlight matching brackets
set ruler " show line and column 
set laststatus=2 ""
set nopaste
set conceallevel=0

"""" Search
set hlsearch " highlight matches
set incsearch " search as characters are entered
set ignorecase
set smartcase

"""" Tree
let g:netrw_liststyle = 3

"""" Look
set title
syntax on " enable syntax
set background=dark

" Things
filetype plugin on

" Lightline
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'powerlineish',
      \ }

" Rainbow
let g:rainbow_active = 1

" Markdown
let g:mkdp_port = '10000'
let g:mkdp_page_title = '${name}'
let g:vim_markdown_conceal = 0
let g:vim_markdown_math = 1
let g:vim_markdown_strikethrough = 1
let g:tex_conceal = ""
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_math = 1

"""" Keybinds

" Mouse shenanigans
if has('mouse')
  set mouse=a
endif

" Nerdtree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" Markdown preview
" nmap <C-s> <Plug>MarkdownPreview
" execute "set <M-s>=\es"
" nmap <M-s> <Plug>MarkdownPreviewStop
" nmap <C-p> <Plug>MarkdownPreviewToggle

" Markdown
" zr: reduces fold level throughout the buffer
" zR: opens all folds
" zm: increases fold level throughout the buffer
" zM: folds everything all the way
" za: open a fold your cursor is on
" zA: open a fold your cursor is on recursively
" zc: close a fold your cursor is on
" zC: close a fold your cursor is on recursively
